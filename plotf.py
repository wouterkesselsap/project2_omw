import matplotlib.pyplot as plt
import numpy as np
import warnings
from scipy.optimize import curve_fit

def angles(angles, N_angles):
    """
    Visualizes the angles of a number of polymers with same length.
    
    Parameters
    ----------
    angles : np.array(N_beads - 2,N_poly)
        angles between three beads
    N_angles : int
        number of possible angles
        
    Raises
    ------
        Average angle
    """
    
    # Line plot
    print('Average angle: {}*pi'.format(np.round(np.mean(angles)/np.pi, 3)))
    plt.plot(angles.T,'lightgrey')
    plt.plot(np.mean(angles,0),'b',label="Average")
    plt.title("Angles between beads for {} polymers of {} beads".format(np.size(angles,0), np.size(angles,1)+2))
    plt.xlabel("Number of beads")
    plt.ylabel("Angle [rad]")
    plt.legend()
    plt.show()

    # Histogram
    plt.hist(angles.flatten(), N_angles-1, density=True, facecolor='g', alpha=0.75)
    plt.title("Angles between beads for {} polymers of {} beads".format(np.size(angles,0), np.size(angles,1)+2))
    plt.xlabel("Angles [rad]")
    plt.show()
    

def polymer(X):
    """
    Visualizes the polymer by plotting and connecting the individual beads.
    
    Parameters
    ----------
    X:  np.array(N, 2)
        x, y coordinate of every last bead
    """
    
    plt.figure(figsize=[13,7])
    plt.plot(X.transpose()[0], X.transpose()[1],marker='o')
    plt.title('Example polymer with {} beads'.format(X.shape[0]))
    plt.xlabel('x-direction [$\\delta$]')
    plt.ylabel('y-direction [$\\delta$]')
    plt.show()


def ends(X, N, mark_centroid=True):
    """
    Plots the location of every last bead of every polymer, where all polymers have equal length N.
    
    Parameters
    ----------
    X : np.array(N, 2)
        x, y coordinate of every last bead
    N : int
        number of beads per polymer, length of polymer
    mark_centroid : bool
        plot centroid of all ends
    
    Returns
    -------
    centroid : np.array(1, 2)
        average end point of all polymers
    """
    
    centroid = np.mean(X, axis=0)
    X = np.asarray(X).transpose()
    plt.figure(figsize=[6,6])
    plt.scatter(X[0], X[1], alpha=0.4, label='Ends')
    if mark_centroid:
        plt.scatter([centroid[0]], [centroid[1]], marker='x', s=200, color='red', label='Centroid')
    plt.plot(np.array([np.min(X[0])-5, np.max(X[0])+5]), np.array([0, 0]), '--k')
    plt.plot(np.array([0, 0]), np.array([np.min(X[1])-5, np.max(X[1])+5]), '--k')
    plt.xlim(np.min(X[0])-5, np.max(X[0])+5)
    plt.ylim(np.min(X[1])-5, np.max(X[1])+5)
    plt.title('Locations of last beads ({} beads per polymer)'.format(N))
    plt.xlabel('x [$\\delta$]')
    plt.ylabel('y [$\\delta$]')
    plt.legend()
    plt.show()
    
    # return
    return centroid
    

def e2e(X, N, bins='auto'):
    """
    Plots histogram of the end-to-end distance per polymer, where all polymers have equal length N.
    
    Parameters
    ----------
    X : numpy array
        end-to-end distances of polymers
    N : int
        number of beads per polymer, length of polymer
    bins : int
        number of bins for histogram
    """
    
    if bins == 'auto':
        if X.shape[0] >= 100:
            plt.hist(X, int(np.round(X.shape[0]/10)), facecolor='green', alpha=0.75)
        else:
            plt.hist(X, 10, facecolor='green', alpha=0.75)
    else:
        plt.hist(X, bins, facecolor='green', alpha=0.75)
    plt.grid(True)
    plt.title('Histogram of end-to-end distances ({} beads per polymer)'.format(N))
    plt.xlabel('End-to-end distances of polymer')
    plt.ylabel('Count')
    plt.show()

    
def e2e_N(N, X, X_std, perm, interactions, fit=False):
    """
    Plots the mean end-to-end distance per polymer length.
    
    Parameters
    ----------
    N : int, or list, or numpy array
        number(s) of beads per polymer
    X : (1, N) numpy array or list, or (2, N) numpy array or list
        mean end-to-end distance, corresponding to polymer lengths of N
    X_std : (1, N) numpy array or list, or (2, N) numpy array or list
        standard deviation of end-to-end distance, corresponding to polymer lengths of N
    perm : bool, or list with boolean(s), or tuple with boolean(s)
        plot according to PERM (True) or Rosenbuth (False)
    interactions : bool
        plot according to Rosenbluth (True) or random walk (False)
    fit : bool
        fit a*N**b through data according to least squares error
         
    Returns
    -------
    popt : list
        fit parameters a, b
    """
    
    X = np.asarray(X)
    popts = []
    for iperm, do_perm in enumerate(perm):
        col = 'blue'
        if do_perm:
            n = N
            col = 'red'
            perm_data = plt.scatter(n, X[iperm], color=col, label='Data (PERM)')
            plt.errorbar(n, X[iperm], X_std[iperm], ls = "none",color=col, capsize=3, label='Std (PERM)')
        elif not do_perm:
            if interactions:
                n = np.asarray(range(3, np.max(N)+1))
                ros_data = plt.plot(n, X[iperm], color=col, label='Data (Rosenbluth)')
                plt.fill_between(n, X[iperm]-X_std[iperm], X[iperm]+X_std[iperm], alpha = 0.2, color= col, label='Std (Rosenbluth)')
            elif not interactions:
                n = np.asarray(range(3, np.max(N)+1))
                ros_data = plt.plot(n, X[iperm], color=col, label='Data (Ideal chain)')
                plt.fill_between(n, X[iperm]-X_std[iperm], X[iperm]+X_std[iperm], alpha = 0.2, color= col, label='Std (Ideal chain)')
        plt.title('Average end-to-end distance per polymer length')
        plt.xlabel('Polymer length $N$ [$\\delta$]')
        plt.ylabel('Distance $R$ [$\\delta$]')

        if fit:
            n = np.asarray(n)
            def f(x, a, b):
                return a * x**b
            popt, pcov = curve_fit(f, n, X[iperm])   # Fit the curve with function f through the data
            x_fit = np.linspace(1, np.ndarray.max(n), 100)
            fit_data = f(x_fit, *popt)
            if do_perm:
                perm_fit = plt.plot(x_fit, fit_data, '--', color=col, label='LSQ fit (PERM)')  # Plot the fitted curve
                print("LSQ fit (PERM)      : f(x) = {} * x ^ {}".format(np.round(popt[0], 2), np.round(popt[1], 2)))
            elif not do_perm:
                if interactions:
                    ros_fit = plt.plot(x_fit, fit_data, '--', color=col, label='LSQ fit (Rosenbluth)')
                    print("LSQ fit (Rosenbluth): f(x) = {} * x ^ {}".format(np.round(popt[0], 2), np.round(popt[1], 2)))
                elif not interactions:
                    ros_fit = plt.plot(x_fit, fit_data, '--', color=col, label='LSQ fit (Ideal chain)')
                    print("LSQ fit (Ideal chain): f(x) = {} * x ^ {}".format(np.round(popt[0], 2), np.round(popt[1], 2)))
            popts.append(popt)
                
    plt.legend()
    plt.show()
    
    # return
    return popts


def energy(N, U, U_std, perm, interactions):
    """
    Plots mean energy per polymer length.
    
    Parameters
    ----------
    N : int, or list, or numpy array
        number(s) of beads per polymer
    U : (1, N) numpy array or list, or (2, N) numpy array or list
         mean energy, corresponding to polymer lengths of N
    U_std : (1, N) numpy array or list, or (2, N) numpy array or list
         std of the energy, corresponding to polymer lengths of N
    perm : bool, or list with boolean(s), or tuple with boolean(s)
        plot according to PERM (True) or Rosenbuth (False)
    interactions : bool
        only plot if interactions are taken into account
    """
    
    U = np.asarray(U)
    U_std = np.asarray(U_std)
    
    if interactions:
        for iperm, do_perm in enumerate(perm):
            col = 'blue'
            if do_perm:
                n = N
                col = 'red'
                perm_data = plt.scatter(n, U[iperm], color=col, label='Data (PERM)')
                plt.errorbar(n, U[iperm], U_std[iperm], ls = "none",color=col, capsize=3, label='Std (PERM)')
            elif not do_perm:
                n = np.asarray(range(3, np.max(N)+1))
                ros_data = plt.plot(n, U[iperm], color=col, label='Data (Rosenbluth)')
                plt.fill_between(n, U[iperm]-U_std[iperm], U[iperm]+U_std[iperm], alpha = 0.2, color= col, label='Std (Rosenbluth)')
            plt.title('Average energy per polymer length')
            plt.xlabel('Polymer length $N$ [$\\delta$]')
            plt.ylabel('Energy $U$ [$\\epsilon$]')

        plt.legend()
        plt.show()
        
        
def weights(W, N, bins = 'auto'):
    """
    Plots histogram of weightfactors of all polymers.
    
    Parameters
    ----------
    W : numpy array
        weightfactors of polymers
    N : int
        number of beads per polymer, length of polymer
    bins : int
        number of bins for histogram
    """
    
    if bins == 'auto':
        if W.shape[0] >= 100:
            plt.hist(W, int(np.round(100)), facecolor='green', alpha=0.75)
        else:
            plt.hist(W, 10, facecolor='green', alpha=0.75)
    else:
        plt.hist(W, bins, facecolor='green', alpha=0.75)
    plt.grid(False)
    plt.title('Histogram of weightfactors ({} beads per polymer)'.format(N))
    plt.xlabel('Weightfactors of polymer')
    plt.ylabel('Count')
    plt.show()
