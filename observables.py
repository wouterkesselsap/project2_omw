import numpy as np

def angles(X):
    """
    Function that calculates the angles between all line pairs connecting three consecutive beads.
    Last modified: 03-04-2019
    
    Parameters
    ----------
    X : np.array(N,D)
        positions of all beads
        
    Returns
    -------
    angles : np.array(1,N-2)
        angles between all line pairs connecting three consecutive beads
    """
    
    dr = X[1:] - X[:-1]                                                     # bead to bead vectors
    dotpr_lines = np.sum(-dr[:-1]*dr[1:], axis = 1)                         # dotproduct between consecutive bead to bead vectors
    dr_mag = np.linalg.norm(dr, axis = 1)                                   # magnitude of bead to bead vectors
    angles = np.arccos(np.round(dotpr_lines/(dr_mag[:-1]*dr_mag[1:]), 4))   # use round to avoid python calculation errors
    
    # return
    return angles


def e2e(X):
    """
    Function that calculates the end-to-end distance of a polymer.
    Last modified: 08-04-2019
    
    Parameters
    ----------
    X : np.array(N,D)
        positions of all beads
        
    Returns
    -------
    d : float
        end-to-end distance of the polymer
    """
            
    d  = np.linalg.norm(X[-1]- X[0])
    
    # return
    return d

def e2e_full(X):
    """
    Function that calculates the distance from the first bead to every other bead of the polymer.
    Last modified: 10-04-2019
    
    Parameters
    ----------
    X : np.array(N,D)
        positions of all beads
                
    Returns
    -------
    d : np.array(1,N)
        distance from the first bead to every other bead of the polymer 
    """
    
    d = np.linalg.norm(X- X[0],axis = 1)    

    # return
    return d


def mean_obs(W,x):
    """
    Function used to calculate the mean of an observable
    Last modified: 10-04-2019
    
    Parameters
    ----------
    W : np.array(1,N-2)
        weightfactor of the polymer
    x : np.array(1,N-2)
        observable
        
    Returns
    -------
    obs_mean : float
        weighted mean of the observable
    """
    
    obs_mean = np.sum(W*x)/np.sum(W)
    
    # return
    return obs_mean


def std_obs(W,x):
    """
    Function used to calculate the standard deviation of an observable
    Last modified: 11-04-2019
    
    Parameters
    ----------
    W : np.array(1,N-2)
        weightfactor of the polymer
    x : np.array(1,N-2)
        observable
        
    Returns
    -------
    obs_std : float
        weighted standard deviation of the observable
    """
    
    obs_var = mean_obs(W,x**2) - mean_obs(W,x)**2
    obs_std = np.sqrt(obs_var)
    
    # return
    return obs_std


def mean_obs_full(W,x):
    """
    Function used to calculate the mean of an observable
    Last modified: 12-04-2019
    
    Parameters
    ----------
    W : np.array(N_poly,N-2)
        weightfactor of the polymer
    x : np.array(N_poly,N-2)
        observable
        
    Returns
    -------
    obs_mean : np.array(1,N-2)
        weighted mean of the observable
    """
    
    obs_mean = np.sum(W*x, axis = 0)/np.sum(W, axis = 0)
    
    # return
    return obs_mean


def std_obs_full(W,x):
    """
    Function used to calculate the standard deviation of an observable
    Last modified: 11-04-2019
    
    Parameters
    ----------
    W : np.array(N_poly,N-2)
        weightfactor of the polymer
    x : np.array(N_poly,N-2)
        observable
        
    Returns
    -------
    obs_std : np.array(1,N-2)
        weighted standard deviation of the observable
    """
    
    obs_var = mean_obs_full(W,x**2) - mean_obs_full(W,x)**2
    obs_std = np.sqrt(obs_var)
    
    # return
    return obs_std