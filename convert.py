""" Non-tunable parameters for conversion between physical and natural units """

sigma    = 0.8                      # Natural scalefactor for distance (meters)
kb       = 1.381E-23                # Boltzmann constant (Joule/Kelvin)
epsilon  = 0.25*kb                  # Natural units for energy (Joule)
T_ref    = epsilon/kb               # Reference temperature (Kelvin)
