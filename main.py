"""
Does calculations for Monte Carlo simulation of polymers.
Takes input parameters from execute.ipynb and constants from init.convert.py.
"""

import numpy as np
import warnings
import matplotlib.pyplot as plt

from convert import *
import build_polymer
import observables
import plotf

def main(N_beads, N_poly, T,
         N_angles, upplim,
         interactions, perm, plots):
    """
    Simulates all different polymers, performs all experiments, calculates observables and plots the results.
    
    Parameters:
    -----------
    N_beads : int, or list, or np.array
        number(s) of beads per polymer
    N_poly : int
        number of polymers to simulate per polymer length
    T : float
        temperature in physical units
    N_angles : int
        number of angles to choose from for subsequent bead location
    upplim : int
        upper limit in PERM algorithm
    interactions : bool
        construct polymer according to Lennard-Jones potential
    perm : bool, or list with boolean(s), or tuple with boolean(s)
        build polymer according to PERM (True) or Rosenbuth (False)
    plots : dict
        what results to plot, contains:
        verbose : bool
            print progress along the way
        N_poly : int
            number of example polymers to plot per polymer length
        example : bool
            plot example polymers
        single : bool
            plot results for observables for per polymer lengths
        all : bool
            plot results for observables over all polymer lengths
        e2e_fit : bool
            fit least quares relation through mean end-to-end distances as a function of polymer length
        centroid : bool
            mark centroid in end locations scatter plot   
    """

    
    """ ------ INITIALIZATION ------ """
    
    # Check type of N_beads and convert into list
    if isinstance(N_beads, int):
        N_beads = [N_beads]
    elif isinstance(N_beads, np.int64):
        N_beads = [N_beads.item()]
    elif isinstance(N_beads, np.ndarray):
        N_beads = np.round(N_beads).astype(int).tolist()
    elif isinstance(N_beads, range):
        N_beads = list(N_beads)
    if np.min(N_beads) < 3:
        raise ValueError('Polymer must be at least 3 beads long.')
    
    # Check type of perm and convert into list
    if isinstance(perm, bool):
        perm = [perm]
    elif isinstance(perm, tuple):
        perm = list(perm)
        
    # Check if perm matches interaction
    if not interactions:
        if True in perm and False in perm:
            perm = [False]
            print("PERM can only be used if interactions are taken into account. PERM is set to False.")
        elif False not in perm:
            raise ValueError('PERM can not be used without interactions.')        
        
    
    # Initialize data structures of observables
    if len(perm) == 1:
        e2e_N = [[]]     # Average end-to-end distance per polymer length
        U_N = [[]]       # Average energy per polymer length
        e2e_N_std =[[]]  # Standard deviation of end-to-end distance per polymer length
        U_N_std = [[]]   # Standard deviation of energy per polymer length
    elif len(perm) == 2:
        e2e_N = [[], []]
        U_N = [[], []]
        e2e_N_std = [[], []]
        U_N_std = [[], []]
    else:
        raise ValueError('Give one ore two booleans for PERM.')
    
    if plots["single"]:
        plots["verbose"] = True
    
    for iperm, do_perm in enumerate(perm):
        if (do_perm and plots["verbose"]):
            print("\n\n----- Building method: PERM -----")
        elif (not do_perm and plots["verbose"]):
            print("\n\n----- Building method: Rosenbluth -----")
        
        # Determine which polymer length(s) is/are needed to simulate
        if do_perm:
            N_beads_calc = N_beads
        elif not do_perm:
            N_beads_calc = [np.max(N_beads)]
            e2e_N_full = []
        
        for N in N_beads_calc:
            if plots["verbose"]:
                print("\n\n{} beads per polymer:".format(N))

            # Initialize
            angles = np.zeros([N_poly,N-2])
            ends = []
            e2e = []
            U = []
            W = []
            X = build_polymer.initialize(N)
            W_start = []
            U_start = []
            if do_perm:
                W_upplim, W_lowlim = build_polymer.init_perm(X, N, T/T_ref, N_angles,
                                                             sigma, interactions, upplim)
            elif not do_perm:
                W_upplim = []
                W_lowlim = []


            """ ------ CALCULATIONS ------ """

            # Build polymer
            for i in range(N_poly):
                poly, W_arr, m_perm, W_start, U_arr, U_start = build_polymer.build_polymer(N, X, T/T_ref, N_angles, 
                                                                                           sigma, interactions, 
                                                                                           do_perm, W_lowlim, W_upplim,
                                                                                           W_start, U_start)
                if do_perm:
                    U = np.append(U, np.sum(U_arr))
                    W = np.append(W, W_arr[-1])
                elif not do_perm:
                    U = np.append(U, np.cumsum(U_arr))
                    W = np.append(W, W_arr)
                    
                X = poly[:m_perm+2]
                
                # Calculate observables
                ends.append(poly[N-1])
                angles[i] = observables.angles(poly)
                if do_perm:
                    e2e.append(observables.e2e(poly))
                if not do_perm:
                    e2e = observables.e2e_full(poly)
                    e2e_N_full.append(e2e[2:])


                """ ------ OUTPUT ------ """

                if plots["N_poly"] > N_poly:
                    warnings.warn("Number of polymers to plot ({}) is greater than number of polymers ({}),\n"
                                  "N_plot is set to {}.".format(plots["N_poly"], N_poly, N_poly))
                    plots["N_poly"] = N_poly

                if (plots["example"] and i < plots["N_poly"]):
                    plotf.polymer(poly)  # Plot first N_plot polymers
            
            if do_perm:
                U_N[iperm].append(observables.mean_obs(W,U))
                e2e_N[iperm].append(observables.mean_obs(W,np.asarray(e2e)))
                e2e_N_std[iperm].append(observables.std_obs(W,np.asarray(e2e)))
                U_N_std[iperm].append(observables.std_obs(W,U))
            elif not do_perm:
                e2e_N_full = np.asarray(e2e_N_full)
                W = np.asarray(W)
                U = np.asarray(U)
                e2e_N[iperm] = observables.mean_obs_full(W.reshape(N_poly,N-2), np.reshape(e2e_N_full,(N_poly,N-2)))
                U_N[iperm] = observables.mean_obs_full(W.reshape(N_poly,N-2), np.reshape(U,(N_poly,N-2)))
                e2e_N_std[iperm] = observables.std_obs_full(W.reshape(N_poly,N-2), np.reshape(e2e_N_full,(N_poly,N-2)))
                U_N_std[iperm] = observables.std_obs_full(W.reshape(N_poly,N-2), np.reshape(U,(N_poly,N-2)))

            # Plot observables
            if plots["single"]:
                _ = plotf.ends(ends, N, mark_centroid=plots["centroid"])
                _ = plotf.angles(angles, N_angles)

                if do_perm:
                    plotf.e2e(e2e, N, bins='auto')
                    if interactions:
                        plotf.weights(W, N, bins = 'auto')
                elif not do_perm:
                    plotf.e2e(e2e_N_full.reshape(N_poly,N-2).transpose()[-1], N, bins = 'auto')
                    if interactions:
                        plotf.weights(W.reshape(N_poly,N-2).transpose()[-1], N, bins = 'auto')
    
    if plots["all"]:
        if (len(N_beads) == 1 and True in perm):
            print("\n\nOnly one polymer length given, final plots over all lengths not realized.")
        else:
            print("\n\nAll polymer lengths:")
            plotf.e2e_N(N_beads, e2e_N, e2e_N_std, perm, interactions, fit=plots["e2e_fit"])
            plotf.energy(N_beads, U_N, U_N_std, perm, interactions)
                