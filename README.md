## Short description
This script and report is written for the second project of the course Computational Physics at the faculty of Applied Physics, TU Delft.

## Getting started
execute.ipynb runs the complete script be running main.py. In execute the plots are shown. The following can be set in execute:
* parameters (Number of beads, number of polymers per bead length, temperature, number of possible angles, upper limit in PERM algorithm),
* system settings (with or withour interactions, PERM/Rosenbluth or both), and
* what plots to show.

## Authors
* Olaf C. Dreijer (4296621)
* Maxim Q. Capelle (4268628), and
* Wouter Kessels (4201248).