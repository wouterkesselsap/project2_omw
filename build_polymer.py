import numpy as np
from numpy.random import choice
import random

def initialize(N):
    """
    Locates first bead at the origin [0,0] and the second bead at [1,0].
    
    Parameters
    ----------
    N : int
        Number of beads in polymer
         
    Returns
    -------
    X : np.array([N,2])
        Positions (x,y) of first two beads
         
    Raises
    ------
    ValueError
        "A POLYmer should consist of at least two beads." when N < 2
    """
    
    if N < 2:
        raise ValueError("A POLYmer should consist of at least two beads.")
    X = np.array([[0,0],
                  [1,0]])
    return X

def energy(X, T, n, sigma, interactions):
    """
    Function that calculates the (possibly unnormalized) probabilities 
    of allowed angle configurations for the new bead. These probabilities 
    are based on the energies of the corresponding positions of the new bead.
    Last modified: 03-04-2019
    
    Parameters
    ----------
    X : np.array(N,2)
        positions of all beads
    T : float
        Temperature
    n : int
        Number of angle configurations
    sigma : float
        constant used in Lennard-Jones potential
    interactions : boolean
        choose interactions or no interactions
        
    Returns
    -------
    P : np.array(1,n)
        Probability of allowed configurations
    theta : np.array(1,n)
        Angles of allowed configurations
    U : np.array(1,n)
        Potential energy of adding a bead to the polymer
    """
    
    theta = np.linspace(0, (2-(1/n))*np.pi, n)              # All angles
    theta_off = np.random.uniform(0,2*np.pi/n)              # Angle offset
    theta += theta_off
    
    
    if interactions: 
        N = X.shape[0]                                      # Number of beads
        D = X.shape[1]                                      # Number of dimensions (2)

        x = X*np.ones([n,N,D])
        x = np.transpose(x, (1,0,2))

        dr_new = np.append(np.cos(theta), np.sin(theta)).reshape(2,n).transpose() # All possible new positions wrt last bead

        r_new = dr_new + X[-1]

        dr = x - r_new                                      # Distances between new bead and all others ([N,n,D] array)
        dr_mag = np.linalg.norm(dr, axis = 2)
        U = 4*((dr_mag/sigma)**(-12) - (dr_mag/sigma)**(-6))
        U = np.sum(U, axis = 0)                             # Potential energy for all allowed configurations
            
        P = np.exp(-U/T)
    
    else:
        P = np.ones(n,)
        U = np.zeros(n,)
    
    # return
    return P, theta, U

def draw(theta_range,pdf):
    """
    Function to choose a theta from theta_range where each theta has a weighted value given in pdf.
    Last modified: 2019/04/02

    Parameters
    ----------
    theta_range : array(1,N_theta)
        range of N_theta theta's to choose from

    pdf : array(1,N_theta)
        probality density function

    Return
    ------
    draw : float
        choosen theta
    """
    
    if np.sum(pdf) != 1:
        if np.sum(pdf) == 0:
            pdf = np.ones(pdf.shape)
        # normalize
        pdf = pdf/np.sum(pdf)
    
    # draw
    draw = choice(theta_range, 1, p=pdf)
    
    # return
    return draw

def add_monomer(X, W, T, n,
                sigma, interactions):
    """
    Function that is used to add a monomer to a polymer. It combines the functions energy and draw.
    
    Parameters
    ----------
    X : (N,2)-array
        initial positions
    W : (1,N)-array
        weightfactors
    T : float
        temperature
    n : int
        number of angles
    sigma : float
        constant used in Lennard-Jones potential
    interactions : boolean
        True: interactions occur
        False: interactions do not occur
    
    Returns
    -------
    X : (N+1,2)-array
        bead positions
    W : float
        Weightfactor
    U_bead : float
        Energy due to addition of a bead
    """
    
    pdf, theta, U = energy(X, T, n, sigma, interactions)
    angle = draw(theta, pdf)
    W = W*pdf[theta == angle]                                                       # Weight of angle configuration
    U_bead = U[theta == angle]

    r_new = X[-1] + np.array([np.cos(angle), np.sin(angle)]).transpose()            # Calculate position of new bead
    X = np.append(X, r_new).reshape(X.shape[0]+1, 2)                                # Create polymer
    
    # return
    return X, W, U_bead

def prune(W_end, W_lowlim, W,
          U, U_bead, i, m_start,
          W_start, X, U_start):
    """
    Function to determine whether polymer will be pruned or not.
    last modified: 10-04-2019
    
    Parameters
    ----------
    W_end : float
        Weightfactor of last state of the polymer
    W_lowlim : (1,N) - array
        Lower limit of weightfactor
    W : (1,N) - array
        Weightfactor
    U : (1,N) - array
        Potential energies of each bead
    U_bead : float
        Potential energy of last bead
    i : int
        Integer used in loop for creating the polymer
    m_start : float
        Number of beads saved from the enrichment of the last run
    W_start : (1,N) - array
        Weightfactor of the previously enriched polymer
    X : (N,2) - array
        Position matrix of all beads of the polymer
    U_start : (1,N) - array
        Energy of the previously enriched polymer
    
    Returns
    -------
    W : (1,N) - array
        Weightfactor
    U : (1,N) - array
        Potential energies of each bead
    X : (N,2) - array
        Position matrix of all beads of the polymer
    i : int
        Integer used in loop for creating the polymer
    W_end : float
        Weightfactor of last state of the polymer
    """
    
    if W_end < W_lowlim[i-2]:
        if random.uniform(0,1) < 0.5:
            # do not prune
            W_end = 2*W_end
            W = np.append(W,W_end)
            U = np.append(U,U_bead)
        else:
            # prune
            i = m_start - 1
            X = X[:m_start]
            W_end = 1
            W = W_start
            U = U_start
    else:
        W = np.append(W,W_end)
        U = np.append(U,U_bead)    
            
    # return
    return W, U, X, i, W_end

def enrich(W, W_upplim, m_start, U):
    """
    Function to determine whether polymer will be enriched or not.
    last modified: 10-04-2019
    
    Parameters
    ----------
    W : (1,N) - array
        Weightfactor
    W_upplim : (1,N) - array
        Upper limit of weightfactor
    m_start : int
        Number of beads saved from the enrichment of the last run
    U : (1,N) - array
        Potential energies of each bead

    Returns
    -------
    W : (1,N) - array
        Weightfactor
    W_start : (1, N) - array
        Initial weightfactor. Nonempty when enriched
    m : int
        States until where the polymer is enriched
    U_start : (1,N) - array
        Energy. Nonempty when enriched
    """
    
    a = W/W_upplim
    
    if np.max(a)>1:
        # enrichment
        m = np.argmax(a)+1
                
        if m>(m_start-2):
            W_start = W[:m]/2
            U_start = U[:m]
            W = W/2
        else:
            m = 0
            W_start = ([])
            U_start = ([])
    else:
        # continue
        m = 0
        W_start = ([])
        U_start = ([])
        
    # return
    return W, W_start, m, U_start

def init_perm(X, N, T, n, sigma,
              interactions, upplim):
    """ 
    Function to initialize the upper and lower limits of the weightfactor in the PERM algorithm.
    Last modified: 09-04-2019
    
    Parameters
    ----------
    X : (2,2) - array
        Positions of all beads before completing the polymer
    N : int
        number of beads to initialize polymer
    T : float
        Temperature
    n : int
        number of angles
    sigma : float
        constant used in Lennard-Jones potential
    interactions : boolean
        define whether interactions occur (true) or not (false)
    upplim : float
        multiplied with the mean weightfactor to obtain the upper limit of W
    
    Returns
    -------
    W_upplim : (1, N-2) - array
        Upper limit of weightfactor
    W_lowlim : (1, N-2) - array
        Lower limit of weightfactor
    """
    
    W_mean = 0
    N_mean = 10                        # number of polymers over which the average weightfactor should be taken
    x0 = X
    
    for j in range(N_mean):            # loop several times to average weightfactor
        W_end = 1
        W_arr = ([])
        
        for i in range(X.shape[0], N):
            X, W_end, U_bead = add_monomer(X, W_end, T, n, sigma, interactions)
            W_arr = np.append(W_arr, W_end)
            
        X = x0
        W_mean += W_arr
    
    W_mean = W_mean/N_mean
    W_upplim = W_mean*upplim
    W_lowlim = W_upplim/10
    
    # return
    return W_upplim, W_lowlim

def build_polymer(N, X, T, n, sigma,
                  interactions, perm,
                  W_lowlim, W_upplim,
                  W_start, U_start):
    """
    Function to build a polymer.
    Last modified: 10-04-2019
    
    Parameters
    ----------
    N : int
        number of beads to initialize polymer
    X : (?,2) - array
        Positions of all beads before completing the polymer
    T : float
        Temperature
    n : int
        number of angles
    sigma : float
        constant used in Lennard-Jones potential
    interactions : boolean
        interactions? true or false
    perm : boolean
        States whether to use the PERM algortihm or not
    W_lowlim : (1, N-2) - array
        Lower limit of weightfactor. Chance of pruning if W becomes lower than this limit
    W_upplim : (1, N-2) - array
        Upper limit of weightfactor. Use enriche if W becomes larger than this limit
    W_start : (1, ?) - array
        Initial weightfactor. Used when enriched
    U_start : (1, ?) - array
        Initial energy configuration. Used when enriched
    
    Returns
    -------
    X : (N,2) - array
        Positions of all beads
    W : (1,N-2)-array
        weightfactors product array
    m : int
        States until where the polymer is enriched
    W_start : (1, ?) - array
        Initial weightfactor. Nonempty when enriched
    U : (1, N-2) - array
        Potential energies for each bead addition
    U_start : (1, ?) - array
        Initial energy configuration. Used when enriched
    """
    
    W_end = 1                                   # Weightfactor (array)
    W = W_start
    m_start = X.shape[0]
    i = m_start
    U = U_start
    while i < N:
        X, W_end, U_bead = add_monomer(X, W_end, T, n, sigma, interactions)
        
        if perm:
            W, U, X, i, W_end = prune(W_end, W_lowlim, W, U, U_bead, i, m_start, W_start, X, U_start)
        else:
            W = np.append(W,W_end)
            U = np.append(U,U_bead)
        
        i += 1

    if perm:
        W, W_start, m, U_start = enrich(W, W_upplim, m_start, U)
    else:
        m = 0
        W_start = ([])
        U_start = ([])
    
    # return
    return X, W, m, W_start, U, U_start